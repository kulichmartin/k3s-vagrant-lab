# k3s-vagrant-lab

This lab contains one master and NUM_NODES worker nodes. The default count is one.
There are more parameters that can be seen in the heading of [Vagrantfile](Vagrantfile).

## Usage
### Start the cluster
1. Set number of nodes:
```export NUM_NODES=2```
2. Start the nodes. This will create `k3s.yaml` file, which holds `kubectl` configuration.
```vagrant up```
3. Set kubeconfig
```export KUBECONFIG=$(pwd)/k3s.yaml
4. Use `kubectl` as usual


### Upsize the cluster
Update `NUM_NODES` and run `vagrant up`


### Access dashboard
1. Get access token
```kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep admin-user | awk '{print $1}')```
2. Access the dashboard at https://10.135.135.101:8443


### Destroy the cluster
```vagrant destroy```