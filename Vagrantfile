BOX = "ubuntu/bionic64"
HOME = File.dirname(__FILE__)
PROJECT = File.basename(HOME)
K3S_VERSION = ENV['K3S_VERSION'] || "v0.7.0"
K3S_CLUSTER_SECRET = "s3cret"
K3S_CLUSTER_NAME = ENV['K3S_CLUSTER_NAME'] || "k3s-vagrant"
NUM_NODES = (ENV['NUM_NODES'] || 1).to_i
NODE_CPUS = (ENV['NODE_CPUS'] || 1).to_i
NODE_MEMORY = (ENV['NODE_MEMORY'] || 512).to_i
NETWORK_PREFIX = ENV['NETWORK_PREFIX'] || "10.135.135"


provision_db = <<SCRIPT
#!/usr/bin/env bash
set -euo pipefail

apt-get update
apt-get install -y docker.io

openssl req -new -text -passout pass:abcd -subj /CN=localhost -out server.req
openssl rsa -in privkey.pem -passin pass:abcd -out server.key
openssl req -x509 -in server.req -text -key server.key -out server.crt
chown 999 server.key  # 999 is postgresql user
chmod 600 server.key
docker run -v $PWD/server.crt:/var/lib/postgresql/server.crt:ro -v $PWD/server.key:/var/lib/postgresql/server.key:ro -e POSTGRES_USER=k3s -e POSTGRES_PASSWORD=k3s -p 5432:5432 -d postgres:11 -c ssl=on -c ssl_cert_file=/var/lib/postgresql/server.crt -c ssl_key_file=/var/lib/postgresql/server.key
SCRIPT


provision_master = <<SCRIPT
#!/usr/bin/env bash
set -euo pipefail

# Restore k3s config if available
if [ -d /vagrant/k3s ]; then
  echo "Restoring k3s config"
  mkdir -p /var/lib/rancher
  cp -R /vagrant/k3s /var/lib/rancher
fi

echo "Installing K3s"
curl -sfL https://get.k3s.io | INSTALL_K3S_VERSION=#{K3S_VERSION} K3S_CLUSTER_SECRET=#{K3S_CLUSTER_SECRET} INSTALL_K3S_EXEC="--disable-agent --storage-endpoint=postgres://k3s:k3s@10.135.135.99:5432/k3s --tls-san #{NETWORK_PREFIX}.100 --node-ip #{NETWORK_PREFIX}.100 --flannel-iface enp0s8" sh -

kubectl config set clusters.default.server "https://#{NETWORK_PREFIX}.100:6443"
sed -i -e 's/default/#{K3S_CLUSTER_NAME}/g' /etc/rancher/k3s/k3s.yaml
cp /etc/rancher/k3s/k3s.yaml /vagrant/k3s.yaml

chown root: /tmp/manifests/*
mv /tmp/manifests/* /var/lib/rancher/k3s/server/manifests/
SCRIPT


provision_node = <<SCRIPT
#!/usr/bin/env bash
set -euo pipefail

echo "Installing K3s"
curl -sfL https://get.k3s.io | INSTALL_K3S_VERSION=#{K3S_VERSION} K3S_URL=https://#{NETWORK_PREFIX}.100:6443 K3S_CLUSTER_SECRET=#{K3S_CLUSTER_SECRET} INSTALL_K3S_EXEC="--flannel-iface enp0s8" sh -
SCRIPT


Vagrant.configure("2") do |config|
  config.vm.provider "virtualbox" do |v|
    v.cpus = NODE_CPUS
    v.memory = NODE_MEMORY
    v.customize ["modifyvm", :id, "--audio", "none"]
    v.customize ["modifyvm", :id, "--cpuexecutioncap", "50"]
  end

  config.vm.box = BOX
  config.vm.hostname = PROJECT


  config.vm.define "db" do |node|
      node.vm.network "private_network", ip: "#{NETWORK_PREFIX}.99"
      node.vm.hostname = "#{PROJECT}-db"
      node.vm.provision "shell", inline: provision_db
  end

  config.vm.define "master" do |node|
      node.vm.network "private_network", ip: "#{NETWORK_PREFIX}.100"
      node.vm.hostname = "#{PROJECT}-master"
      node.vm.provision "file", source: "manifests", destination: "/tmp/manifests"
      node.vm.provision "shell", inline: provision_master
  end

  (1..NUM_NODES).each do |i|
    config.vm.define ".#{i}" do |node|
      node.vm.network "private_network", ip: "#{NETWORK_PREFIX}.#{100+i}"
      node.vm.hostname = "#{PROJECT}-#{i}"
      node.vm.provision "shell", inline: provision_node
    end
  end
end
